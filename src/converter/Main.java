package converter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("converter.fxml"));
        primaryStage.getIcons().add(new Image("converter\\img\\converter.png"));
        primaryStage.setTitle("Конвертер");
        primaryStage.setResizable(false);
        Scene scene = new Scene(root, 544, 393);
        scene.getStylesheets().add(getClass().getResource("styles\\styles.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}