package converter;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ConverterController {

    public static final String NOT_A_NUMBER = "Введите числа";
    public static final String EMPTY_FIELD = "Пустое поле";
    public static final String MAKE_CHOICE = "Не выбрана единица измерения";
    public static final String NEGATIVE_NUMBER = "Введите число больше нуля";

    @FXML
    private TextField firstLengthTextField;

    @FXML
    private TextField secondLengthTextField;

    @FXML
    private ComboBox<String> firstLengthComboBox;

    @FXML
    private ComboBox<String> secondLengthComboBox;

    @FXML
    private Label lengthErrorLabel;

    @FXML
    private TextField firstWeightTextField;

    @FXML
    private TextField secondWeightTextField;

    @FXML
    private ComboBox<String> firstWeightComboBox;

    @FXML
    private ComboBox<String> secondWeightComboBox;

    @FXML
    private Label weightErrorLabel;

    @FXML
    private TextField firstCurrencyTextField;

    @FXML
    private TextField secondCurrencyTextField;

    @FXML
    private ComboBox<String> firstCurrencyComboBox;

    @FXML
    private ComboBox<String> secondCurrencyComboBox;

    @FXML
    private Label currencyErrorLabel;

    @FXML
    private TextField firstVolumeTextField;

    @FXML
    private TextField secondVolumeTextField;

    @FXML
    private ComboBox<String> firstVolumeComboBox;

    @FXML
    private ComboBox<String> secondVolumeComboBox;

    @FXML
    private Label volumeErrorLabel;

    @FXML
    int choiceLengthFirstValue() {
        try {
            switch (firstLengthComboBox.getValue()) {
                case "сантиметр":
                    return 1;
                case "метр":
                    return 2;
                case "километр":
                    return 3;
                case "дюйм":
                    return 4;
                case "миля":
                    return 5;
            }
        } catch (Exception e) {
            lengthErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }

    /**
     * Очищает метку с ошибкой
     */
    private void clearErrorLabel(Label label) {
        if (label.getText().isEmpty()) {
        } else label.setText("");
    }

    /**
     * Проверяет числовое поле на наличие введенных данных
     *
     * @return 0, если поле пустое и 1, если в поле явведены данные
     */
    private int checkForEmpty(TextField field, Label label) {
        if (field.getText().isEmpty()) {
            label.setText(EMPTY_FIELD);
            return 0;
        }
        return 1;
    }

    /**
     * Сохраняет введенные в текстовое поле значения
     *
     * @param field текст из текстового поля
     * @return число или -1, если введены нечисловые данные
     */
    private double giveText(TextField field, Label label) {
        try {
            return Double.parseDouble(field.getText());
        } catch (Exception e) {
            label.setText(NOT_A_NUMBER);
        }
        return -1;
    }

    @FXML
    int choiceLengthSecondValue() {
        try {
            switch (secondLengthComboBox.getValue()) {
                case "сантиметр":
                    return 1;
                case "метр":
                    return 2;
                case "километр":
                    return 3;
                case "дюйм":
                    return 4;
                case "миля":
                    return 5;
            }
        } catch (Exception e) {
            lengthErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }


    @FXML
    int choiceVolumeFirstValue() {
        try {
            switch (firstVolumeComboBox.getValue()) {
                case "литр":
                    return 1;
                case "миллилитр":
                    return 2;
                case "унция":
                    return 3;
                case "кубический фунт":
                    return 4;
                case "кубический дюйм":
                    return 5;
            }
        } catch (Exception e) {
            volumeErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }

    @FXML
    int choiceVolumeSecondValue() {
        try {
            switch (secondVolumeComboBox.getValue()) {
                case "литр":
                    return 1;
                case "миллилитр":
                    return 2;
                case "унция":
                    return 3;
                case "кубический фунт":
                    return 4;
                case "кубический дюйм":
                    return 5;
            }
        } catch (Exception e) {
            volumeErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }


    @FXML
    int choiceCurrencyFirstValue() {
        try {
            switch (firstCurrencyComboBox.getValue()) {
                case "доллар":
                    return 1;
                case "евро":
                    return 2;
                case "рубль":
                    return 3;
                case "юань":
                    return 4;
                case "гривна":
                    return 5;
            }
        } catch (Exception e) {
            currencyErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }

    @FXML
    int choiceCurrencySecondValue() {
        try {
            switch (secondCurrencyComboBox.getValue()) {
                case "доллар":
                    return 1;
                case "евро":
                    return 2;
                case "рубль":
                    return 3;
                case "юань":
                    return 4;
                case "гривна":
                    return 5;
            }
        } catch (Exception e) {
            currencyErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }

    @FXML
    int choiceWeightFirstValue() {
        try {
            switch (firstWeightComboBox.getValue()) {
                case "грамм":
                    return 1;
                case "килограмм":
                    return 2;
                case "тонна":
                    return 3;
                case "фунт":
                    return 4;
                case "стон":
                    return 5;
            }
        } catch (Exception e) {
            weightErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }

    @FXML
    int choiceWeightSecondValue() {
        try {
            switch (secondWeightComboBox.getValue()) {
                case "грамм":
                    return 1;
                case "килограмм":
                    return 2;
                case "тонна":
                    return 3;
                case "фунт":
                    return 4;
                case "стон":
                    return 5;
            }
        } catch (Exception e) {
            weightErrorLabel.setText(MAKE_CHOICE);
            return -1;
        }
        return 0;
    }


    /**
     * Вычисляет и выводит результат для единиц длины
     */
    public void giveLengthResult() {
        double length = giveText(firstLengthTextField, lengthErrorLabel);
        if (checkForEmpty(firstLengthTextField, lengthErrorLabel) == 1) {
            if (length != -1) {
                if (length > 0) {
                    if (choiceLengthFirstValue() == 1) {
                        clearErrorLabel(lengthErrorLabel);
                        if (choiceLengthSecondValue() == 1) {
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 2) {
                            length *= 0.01;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 3) {
                            length *= 0.00001;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 4) {
                            length *= 0.39;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 5) {
                            length *= 0.0000062;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                    }
                    if (choiceLengthFirstValue() == 2) {
                        clearErrorLabel(lengthErrorLabel);
                        if (choiceLengthSecondValue() == 1) {
                            length *= 100;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 2) {
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 3) {
                            length *= 0.001;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 4) {
                            length *= 39.37;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 5) {
                            length *= 0.00062;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                    }
                    if (choiceLengthFirstValue() == 3) {
                        clearErrorLabel(lengthErrorLabel);
                        if (choiceLengthSecondValue() == 1) {
                            length *= 100000;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 2) {
                            length *= 1000;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 3) {
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 4) {
                            length *= 39370.08;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 5) {
                            length *= 0.62;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                    }
                    if (choiceLengthFirstValue() == 4) {
                        clearErrorLabel(lengthErrorLabel);
                        if (choiceLengthSecondValue() == 1) {
                            length *= 2.54;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 2) {
                            length *= 0.025;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 3) {
                            length *= 0.000025;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 4) {
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 5) {
                            length *= 0.000016;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                    }
                    if (choiceLengthFirstValue() == 5) {
                        clearErrorLabel(lengthErrorLabel);
                        if (choiceLengthSecondValue() == 1) {
                            length *= 160934.4;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 2) {
                            length *= 1609.34;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 3) {
                            length *= 1.61;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 4) {
                            length *= 63360;
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                        if (choiceLengthSecondValue() == 5) {
                            secondLengthTextField.setText(String.valueOf(length));
                        }
                    }
                } else {
                    lengthErrorLabel.setText(NEGATIVE_NUMBER);
                }
            }
        }
    }

    /**
     * Вычисляет и выводит результат для единиц объема
     */
    public void giveVolumeResult() {
        double volume = giveText(firstVolumeTextField, volumeErrorLabel);
        if (checkForEmpty(firstVolumeTextField, volumeErrorLabel) == 1) {
            if (volume != -1) {
                if (volume > 0) {
                    if (choiceVolumeFirstValue() == 1) {
                        clearErrorLabel(volumeErrorLabel);
                        if (choiceVolumeSecondValue() == 1) {
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 2) {
                            volume *= 1000;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 3) {
                            volume *= 33.81;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 4) {
                            volume *= 0.0063;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 5) {
                            volume *= 61.02;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceVolumeFirstValue() == 2) {
                        clearErrorLabel(volumeErrorLabel);
                        if (choiceVolumeSecondValue() == 1) {
                            volume *= 0.001;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 2) {
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 3) {
                            volume *= 0.034;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 4) {
                            volume *= 0.0000063;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 5) {
                            volume *= 0.061;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceVolumeFirstValue() == 3) {
                        clearErrorLabel(volumeErrorLabel);
                        if (choiceVolumeSecondValue() == 1) {
                            volume *= 0.03;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 2) {
                            volume *= 29.574;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 3) {
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 4) {
                            volume *= 0.00019;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 5) {
                            volume *= 1.8;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceVolumeFirstValue() == 4) {
                        clearErrorLabel(volumeErrorLabel);
                        if (choiceVolumeSecondValue() == 1) {
                            volume *= 158.987;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 2) {
                            volume *= 158987.295;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 3) {
                            volume *= 5376;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 4) {
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 5) {
                            volume *= 9702;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceVolumeFirstValue() == 5) {
                        clearErrorLabel(volumeErrorLabel);
                        if (choiceVolumeSecondValue() == 1) {
                            volume *= 0.016;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 2) {
                            volume *= 16.387;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 3) {
                            volume *= 0.554;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 4) {
                            volume *= 0.0001;
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                        if (choiceVolumeSecondValue() == 5) {
                            secondVolumeTextField.setText(String.valueOf(volume));
                        }
                    }
                } else {
                    volumeErrorLabel.setText(NEGATIVE_NUMBER);
                }
            }
        }
    }

    /**
     * Вычисляет и выводит результат для валюты
     */
    public void giveCurrencyResult() {
        double currency = giveText(firstCurrencyTextField, currencyErrorLabel);
        if (checkForEmpty(firstCurrencyTextField, currencyErrorLabel) == 1) {
            if (currency != -1) {
                if (currency > 0) {
                    if (choiceCurrencyFirstValue() == 1) {
                        clearErrorLabel(currencyErrorLabel);
                        if (choiceCurrencySecondValue() == 1) {
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 2) {
                            currency *= 0.93;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 3) {
                            currency *= 74.43;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 4) {
                            currency *= 7.08;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 5) {
                            currency *= 27.01;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                    }
                    if (choiceCurrencyFirstValue() == 2) {
                        clearErrorLabel(currencyErrorLabel);
                        if (choiceCurrencySecondValue() == 1) {
                            currency *= 1.08;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 2) {
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 3) {
                            currency *= 80.3;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 4) {
                            currency *= 7.63;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 5) {
                            currency *= 29.14;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                    }
                    if (choiceCurrencyFirstValue() == 3) {
                        clearErrorLabel(currencyErrorLabel);
                        if (choiceCurrencySecondValue() == 1) {
                            currency *= 0.0134;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 2) {
                            currency *= 0.0125;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 3) {
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 4) {
                            currency *= 0.094;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 5) {
                            currency *= 0.3593;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                    }
                    if (choiceCurrencyFirstValue() == 4) {
                        clearErrorLabel(currencyErrorLabel);
                        if (choiceCurrencySecondValue() == 1) {
                            currency *= 0.141;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 2) {
                            currency *= 0.131;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 3) {
                            currency *= 10.613;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 4) {
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 5) {
                            currency *= 0.0382;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                    }
                    if (choiceCurrencyFirstValue() == 5) {
                        clearErrorLabel(currencyErrorLabel);
                        if (choiceCurrencySecondValue() == 1) {
                            currency *= 0.037;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 2) {
                            currency *= 0.034;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 3) {
                            currency *= 2.783;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 4) {
                            currency *= 26.203;
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                        if (choiceCurrencySecondValue() == 5) {
                            secondCurrencyTextField.setText(String.valueOf(currency));
                        }
                    }
                } else {
                    currencyErrorLabel.setText(NEGATIVE_NUMBER);
                }
            }
        }
    }

    /**
     * Вычисляет и выводит результат для единиц массы
     */
    public void giveWeightResult() {
        double volume = giveText(firstWeightTextField, weightErrorLabel);
        if (checkForEmpty(firstWeightTextField, weightErrorLabel) == 1) {
            if (volume != -1) {
                if (volume > 0) {
                    if (choiceWeightFirstValue() == 1) {
                        clearErrorLabel(weightErrorLabel);
                        if (choiceWeightSecondValue() == 1) {
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 2) {
                            volume *= 0.001;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 3) {
                            volume *= 0.000001;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 4) {
                            volume *= 0.000061;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 5) {
                            volume *= 0.035;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceWeightFirstValue() == 2) {
                        clearErrorLabel(weightErrorLabel);
                        if (choiceWeightSecondValue() == 1) {
                            volume *= 1000;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 2) {
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 3) {
                            volume *= 0.001;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 4) {
                            volume *= 0.061;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 5) {
                            volume *= 35.27;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceWeightFirstValue() == 3) {
                        clearErrorLabel(weightErrorLabel);
                        if (choiceWeightSecondValue() == 1) {
                            volume *= 1000000;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 2) {
                            volume *= 1000;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 3) {
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 4) {
                            volume *= 61.0501;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 5) {
                            volume *= 35273.962;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceWeightFirstValue() == 4) {
                        clearErrorLabel(weightErrorLabel);
                        if (choiceWeightSecondValue() == 1) {
                            volume *= 16380;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 2) {
                            volume *= 16.38;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 3) {
                            volume *= 0.0164;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 4) {
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 5) {
                            volume *= 577.787;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                    }
                    if (choiceWeightFirstValue() == 5) {
                        clearErrorLabel(weightErrorLabel);
                        if (choiceWeightSecondValue() == 1) {
                            volume *= 28.35;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 2) {
                            volume *= 0.0283;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 3) {
                            volume *= 0.000028;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 4) {
                            volume *= 0.0017;
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                        if (choiceWeightSecondValue() == 5) {
                            secondWeightTextField.setText(String.valueOf(volume));
                        }
                    }
                } else {
                    volumeErrorLabel.setText(NEGATIVE_NUMBER);
                }
            }
        }
    }

    @FXML
    public void lengthClear() {
        clearErrorLabel(lengthErrorLabel);
        firstLengthTextField.clear();
        secondLengthTextField.clear();
    }

    @FXML
    public void weightClear() {
        clearErrorLabel(weightErrorLabel);
        firstWeightTextField.clear();
        secondWeightTextField.clear();
    }

    @FXML
    public void currencyClear() {
        clearErrorLabel(currencyErrorLabel);
        firstCurrencyTextField.clear();
        secondCurrencyTextField.clear();
    }

    @FXML
    public void volumeClear() {
        clearErrorLabel(volumeErrorLabel);
        firstVolumeTextField.clear();
        secondVolumeTextField.clear();
    }
}